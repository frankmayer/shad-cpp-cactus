add_library(libportscan portscan.cpp)
target_link_libraries(libportscan cactus)

add_catch(test_portscan portscan_test.cpp)
target_link_libraries(test_portscan libportscan)

add_executable(portscan main.cpp)
target_link_libraries(portscan libportscan)
