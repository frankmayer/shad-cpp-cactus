enable_language(ASM)

add_library(cactus
  core/fiber.cpp
  core/scheduler.cpp
  core/timer.cpp
  core/debug.cpp
  core/group.cpp
  core/exceptions.cpp
  internal/context.cpp
  internal/context.S
  internal/wait_queue.cpp
  io/view.cpp
  io/reader.cpp
  io/writer.cpp
  sync/mutex.cpp
  net/address.cpp
  net/net.cpp
  net/poller.cpp)

target_link_libraries(cactus
  pthread
  CONAN_PKG::folly
  CONAN_PKG::fmt)

add_catch(test_cactus
  internal/context_test.cpp
  internal/intrusive_list_test.cpp
  internal/wait_queue_test.cpp
  io/io_test.cpp
  io/view_test.cpp
  core/scheduler_test.cpp
  core/fiber_test.cpp
  core/group_test.cpp
  sync/mutex_test.cpp)

target_link_libraries(test_cactus
  cactus)

add_executable(test_cactus_fiber_death
  core/fiber_death_test.cpp)

target_link_libraries(test_cactus_fiber_death
  cactus)

add_test_binary(test_cactus_fiber_death)

add_catch(test_cactus_net
  net/net_test.cpp)

target_link_libraries(test_cactus_net
  cactus)

add_benchmark(benchmark_context internal/context_benchmark.cpp)
target_link_libraries(benchmark_context cactus)

add_benchmark(benchmark_scheduler core/scheduler_benchmark.cpp)
target_link_libraries(benchmark_scheduler cactus)

add_benchmark(benchmark_net_server net/net_server_benchmark.cpp)
target_link_libraries(benchmark_net_server cactus)

add_benchmark(benchmark_net_ping_pong net/net_ping_pong_benchmark.cpp)
target_link_libraries(benchmark_net_ping_pong cactus)

add_subdirectory(rpc)