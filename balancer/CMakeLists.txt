add_library(balancer balancer.cpp)
target_link_libraries(balancer cactus)

add_catch(test_balancer balancer_test.cpp)
target_link_libraries(test_balancer balancer)